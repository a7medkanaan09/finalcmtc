<?php
session_start();
if (isset($_SESSION['views'])) {
    $_SESSION['views']++;
    
} else {
    $_SESSION['views'] =1;
}
?>
<html>
    <head>

        <?php include 'includes/header.php'; ?> 
        
        <?php require_once 'includes/slide.php'; ?>

    </head>


        <div class="container" dir="rtl">
            <div class="row" style="height: 300px;">      
                <br>
                <div class="col-md-4 col-sm-4  img-responsive" dir="rtl">
                    <div class="featurette" id="about">
                        <img class="featurette-image  img-hover img-responsive"  src="img/silver3.png" alt="مكوك البلد">
                        <br>  <br>
                        <div class="col-sm-4">
                            <img class="featurette-image img-hover img-responsive"  src="img/daewoo.jpg" alt="Daewoo">
                        </div>
                        <div class="col-sm-4">
                            <img class="featurette-image img-hover img-responsive"  src="img/mercedes.jpg" alt="Mercedes">
                        </div>
                        <div class="col-sm-4">
                            <img class="featurette-image img-hover img-responsive"  src="img/yutong.jpg" alt=" Yutong">
                        </div>
                    </div></div>

                <div class="col-md-8 col-sm-8 " dir="rtl">
                    <?php require_once 'includes/tab.php'; ?>

                </div>
            </div>


        </div>
        <div class="container  ">
            <div class="row">
            <div class="col-md-pull-4">
                <div class="row" > 
                    <br><br><br><br><br>

                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading" dir="rtl">
                                <h4><i class="fa fa-fw fa-check"></i> الاخبار</h4>
                            </div>
                            <div class="panel-body" style="height: 250px;">
                                <p>
                                    
                                </p>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel panel-default" style="height: 40px;">
                            <div class="panel-heading" dir="rtl">
                                <h4><i class="fa fa-fw fa-gift"></i> المناسبات </h4>
                            </div>

                            <div class="panel panel-default"  >
                                <marquee direction="up" scrollamount="2">
                                    <center>
                                        <div style="height: 250px;">
                                            <?php
                                            $data = new database();
                                            $data->query("SELECT * FROM event");
                                            $row = $data->fetchAll();

                                            foreach ($row as $rowset) {
                                                echo "<b>";
                                                echo $rowset['event_title'];
                                                echo $rowset['event_date'];
                                                echo "<br> <br> <br>";
                                                echo "</b>";
                                            }
                                            ?>   
                                        </div>

                                    </center>
                                </marquee>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 " >
                        <div class="panel panel-default"  >
                            <div class="panel-heading" dir="rtl" >
                                <h4><i class="fa fa-fw fa-compass"></i>الموقع  </h4>
                            </div>
                            <div class="panel-body" dir="rtl" style="height: 250px;"  >
                                  <marquee direction="up" scrollamount="2">
                                      <a href="contact.php" >
                                          <center>
                                              <br><br><br><br><br>
                                              <p style="font-size: 16px;"> <b>الأردن -عمان</b></p> 
                                              <br>
                                              <p style="font-size: 16px;"> <b>عين الباشا</b></p> 
                                          </center>
                                      </a>
                                      
                                  </marquee>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
            </div>
        </div>
        <br><br><br><br>

        <div class="container">
            <div class="row">
                <div class="col-lg-4"   dir="ltr " style="height: 500px" >


                <h2  class="featurette-heading"dir="rtl" style="width: 100%;height: 40px;">مكوك البلد
                    <span class="text-muted">( لف البلد بربع دينار)</span>
                </h2>
                <p class="lead" dir="rtl" style="height: 70px;">اطلقت مبادرة "لف البلد بربع دينار" بالتعاون مع امانة عمان الكبرى وذلك لتسهيل حركة المرور داخل العاصمة عمان
                </p>


                <div class="row "  dir="ltr">
                    <!-- First Featurette -->
                    <div class="featurette" id="about">
                        <a href="mkok.php">
                            <img  data-toggle="tooltip" data-placement="left" title="مكوك البلد" width="auto" height="auto" class="featurette-image img-circle img-responsive pull-right" src="img/mkok4.jpg">
                        </a>
                    </div>
                </div>
            </div>
                <div class="col-lg-4"  dir="rtl">


                <h2  class="featurette-heading" style="width: 100%;height: 40px;">الجامعة الألمانية
                  
                </h2>
                <p class="lead" dir="rtl" style="height: 70px;">
                    "لف البلد بربع دينار" بالتعاون مع امانة عمان الكبرى وذلك لتسهيل حركة المرور داخل العاصمة عمان
                </p>



                <div class="row "  dir="ltr">
                    <div class="featurette" id="about">
                        <a href="mkok.php">
                            <img  data-toggle="tooltip" data-placement="left" title="مكوك البلد" width="400px" height="400px" class="featurette-image img-circle img-responsive pull-right" src="img/mkok4.jpg">
                        </a>
                     </div>
                     </div>
            </div>
                <div class="col-lg-4"  dir="rtl">
                    
                    
                    <h2  class="featurette-heading" style="width: 100%;height: 40px;">الجامعة الألمانية
                  
                </h2>
                <p class="lead" dir="rtl" style="height: 70px;">
                    "لف البلد بربع دينار" بالتعاون مع امانة عمان الكبرى وذلك لتسهيل حركة المرور داخل العاصمة عمان
                </p>



                <div class="row "  dir="rtl">
                    <div class="featurette" id="about">
                        <a href="mkok.php">
                            <img  data-toggle="tooltip" data-placement="left" title="مكوك البلد" width="450px" height="450px" class="featurette-image img-circle img-responsive pull-right" src="img/mkok4.jpg">
                        </a>
                     </div>
                     </div>
                </div>
               
            </div>
     </div>
            
        <br><br><br><br>
        <div class="container">
             <div class="row" style="height: 150px;">
                 <div class=" col-sm-3   " >
              <a class="btn btn-lg btn-default btn-block" href="#">عودة للاعلى </a>
            </div>
                 <div class="col-sm-9">
                    
                 </div>
                
        </div> 
        </div>
        
          
        
   <?php include_once 'includes/footer.php';?>

     

    